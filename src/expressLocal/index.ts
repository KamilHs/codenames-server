import cors from 'cors';
import helmet from 'helmet';
import express from 'express';

import { response } from '../utils/response';
import { app } from '../servers';

app.use(
    cors({
        origin: process.env.ALLOWED_ORIGIN || 'http://localhost:3000',
        credentials: true
    })
);
app.use(helmet());
app.use(express.json());

app.get('/', (req, res, next) => {
    res.locals.data = 'hello world';
    next();
});

// Middleware which formats the response and sends it to the user
app.use((req, res) => {
    const { data, error } = res.locals;
    res.json(response(data, error));
});
